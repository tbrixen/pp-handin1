#!/bin/bash
# Make a plot of the cumulative error function for the error values for
# each of the four mandatory algorithms with five  online  sample
cd out/production/pp-handin1
ln -s ../../../data
ln -s ../../../output

# Generate data in output/
java org.randomgroupname.EmpiricalKNN 1
java org.randomgroupname.EmpiricalKNN 3
java org.randomgroupname.ModelKNN 1
java org.randomgroupname.ModelKNN 3

# Generating score. Destination output/scores
java org.randomgroupname.ScoreNN empirical1NN.txt
java org.randomgroupname.ScoreNN empirical3NN.txt
java org.randomgroupname.ScoreNN model1NN.txt
java org.randomgroupname.ScoreNN model3NN.txt

cd ../../../output/plots/
gnuplot partII-b.gp

echo "Plot is available under output/plots/partII-b.png"
