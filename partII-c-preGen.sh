#!/bin/bash
# Make a plot of the cumulative error function for the error values for
# each of the four mandatory algorithms with five  online  sample

# This files assume that the pre-generated files are present
# in dir /output/empirical{1,..,5}NN-100.txt and model{1,..,5}NN-100.txt
cd out/production/pp-handin1
ln -s ../../../data
ln -s ../../../output

# Generating avarage. Destination output/avg-*NN-100.txt
echo "Making average for empirical"
java org.randomgroupname.Avg empirical1NN-100.txt
java org.randomgroupname.Avg empirical2NN-100.txt
java org.randomgroupname.Avg empirical3NN-100.txt
java org.randomgroupname.Avg empirical4NN-100.txt
java org.randomgroupname.Avg empirical5NN-100.txt

echo "Making average for model"
java org.randomgroupname.Avg model1NN-100.txt
java org.randomgroupname.Avg model2NN-100.txt
java org.randomgroupname.Avg model3NN-100.txt
java org.randomgroupname.Avg model4NN-100.txt
java org.randomgroupname.Avg model5NN-100.txt

# Generating scores. Destination output/scores/avg-*NN-100.txt
echo "Making score for empirical"
java org.randomgroupname.ScoreNN avg-empirical1NN-100.txt >/dev/null
java org.randomgroupname.ScoreNN avg-empirical2NN-100.txt >/dev/null
java org.randomgroupname.ScoreNN avg-empirical3NN-100.txt >/dev/null
java org.randomgroupname.ScoreNN avg-empirical4NN-100.txt >/dev/null
java org.randomgroupname.ScoreNN avg-empirical5NN-100.txt >/dev/null

echo "Making scores for model"
java org.randomgroupname.ScoreNN avg-model1NN-100.txt > /dev/null
java org.randomgroupname.ScoreNN avg-model2NN-100.txt > /dev/null
java org.randomgroupname.ScoreNN avg-model3NN-100.txt > /dev/null
java org.randomgroupname.ScoreNN avg-model4NN-100.txt > /dev/null
java org.randomgroupname.ScoreNN avg-model5NN-100.txt > /dev/null

cd ../../../output/plots/
gnuplot partII-c.gp
gnuplot partII-c-m.gp
gnuplot partII-c-e.gp

echo "Plot is available under output/plots/ for files partII-c.png, partII-c-m.png partII-c-e.png"

