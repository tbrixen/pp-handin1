#!/bin/bash
# Make a plot of the cumulative error function for the error values for
# each of the four mandatory algorithms with five  online  sample
cd out/production/pp-handin1
ln -s ../../../data
ln -s ../../../output

# Variables
EK1=../../../output/empirical1NN-100.txt
EK2=../../../output/empirical2NN-100.txt
EK3=../../../output/empirical3NN-100.txt
EK4=../../../output/empirical4NN-100.txt
EK5=../../../output/empirical5NN-100.txt
MK1=../../../output/model1NN-100.txt
MK2=../../../output/model2NN-100.txt
MK3=../../../output/model3NN-100.txt
MK4=../../../output/model4NN-100.txt
MK5=../../../output/model5NN-100.txt


# Generate data. Destination output/*NN-100.txt
rm $EK1;
for i in `seq 1 100`
do
	echo "empirical K=1 at " $i "th iteration"
	java org.randomgroupname.EmpiricalKNN 1 >> $EK1
done
rm $EK2;
for i in `seq 1 100`
do
	echo "empirical K=2 at " $i "th iteration"
	java org.randomgroupname.EmpiricalKNN 2 >> $EK2
done
rm $EK3;
for i in `seq 1 100`
do
	echo "empirical K=3 at " $i "th iteration"
	java org.randomgroupname.EmpiricalKNN 3 >> $EK3
done
rm $EK4;
for i in `seq 1 100`
do
	echo "empirical K=4 at " $i "th iteration"
	java org.randomgroupname.EmpiricalKNN 4 >> $EK4
done

rm $EK5;
for i in `seq 1 100`
do
	echo "empirical K=5 at " $i "th iteration"
	java org.randomgroupname.EmpiricalKNN 5 >> $EK5
done

rm $MK1;
for i in `seq 1 100`
do
	echo "model K=1 at " $i "th iteration"
	java org.randomgroupname.ModelKNN 1 >> $MK1
done
rm $MK2;
for i in `seq 1 100`
do
	echo "model K=2 at " $i "th iteration"
	java org.randomgroupname.ModelKNN 2 >> $MK2
done
rm $MK3;
for i in `seq 1 100`
do
	echo "model K=3 at " $i "th iteration"
	java org.randomgroupname.ModelKNN 3 >> $MK3
done

rm $MK4;
for i in `seq 1 100`
do
	echo "model K=4 at " $i "th iteration"
	java org.randomgroupname.ModelKNN 4 >> $MK4
done

rm $MK5;
for i in `seq 1 100`
do
	echo "model K=5 at " $i "th iteration"
	java org.randomgroupname.ModelKNN 5 >> $MK5
done

# Generating avarage. Destination output/avg-*NN-100.txt
echo "Making average for empirical"
java org.randomgroupname.Avg empirical1NN-100.txt
java org.randomgroupname.Avg empirical2NN-100.txt
java org.randomgroupname.Avg empirical3NN-100.txt
java org.randomgroupname.Avg empirical4NN-100.txt
java org.randomgroupname.Avg empirical5NN-100.txt

echo "Making average for model"
java org.randomgroupname.Avg model1NN-100.txt
java org.randomgroupname.Avg model2NN-100.txt
java org.randomgroupname.Avg model3NN-100.txt
java org.randomgroupname.Avg model4NN-100.txt
java org.randomgroupname.Avg model5NN-100.txt

# Generating scores. Destination output/scores/avg-*NN-100.txt
echo "Making score for empirical"
java org.randomgroupname.ScoreNN avg-empirical1NN-100.txt >/dev/null
java org.randomgroupname.ScoreNN avg-empirical2NN-100.txt >/dev/null
java org.randomgroupname.ScoreNN avg-empirical3NN-100.txt >/dev/null
java org.randomgroupname.ScoreNN avg-empirical4NN-100.txt >/dev/null
java org.randomgroupname.ScoreNN avg-empirical5NN-100.txt >/dev/null

echo "Making scores for model"
java org.randomgroupname.ScoreNN avg-model1NN-100.txt > /dev/null
java org.randomgroupname.ScoreNN avg-model2NN-100.txt > /dev/null
java org.randomgroupname.ScoreNN avg-model3NN-100.txt > /dev/null
java org.randomgroupname.ScoreNN avg-model4NN-100.txt > /dev/null
java org.randomgroupname.ScoreNN avg-model5NN-100.txt > /dev/null

cd ../../../output/plots/
gnuplot partII-c.gp
gnuplot partII-c-m.gp
gnuplot partII-c-e.gp

echo "Plot is available under output/plots/ for files partII-c.png, partII-c-m.png partII-c-e.png"

