#!/bin/bash
# Make a plot of the cumulative error function for the error values for
# each of the four mandatory algorithms with five  online  sample
cd out/production/pp-handin1
ln -s ../../../data
ln -s ../../../output

# Generate data
java org.randomgroupname.SignalStrength

cd ../../../output/scores

# Format data
sort --version-sort signalStrAvg.txt > SSAvg.txt
sort --version-sort signalStrMax.txt > SSMax.txt
sort --version-sort signalStrMin.txt > SSMin.txt

rm signalStrAvg.txt
rm signalStrMax.txt
rm signalStrMin.txt

cd ../plots/

# Generate plots
gnuplot partII-a.gp

echo "Plot is available under output/plots/partII-a.png"
