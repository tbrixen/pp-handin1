set title "Cumulative error function for 100 iterations for model and empirical"
set autoscale
set grid
set style data linespoints
set key reverse Left outside
set terminal png size 1000,500 
set output "../plots/partII-c.png"
plot '../scores/avg-empirical1NN-100.txt' , \
     '../scores/avg-empirical2NN-100.txt' , \
     '../scores/avg-empirical3NN-100.txt' , \
     '../scores/avg-empirical4NN-100.txt' , \
     '../scores/avg-empirical5NN-100.txt' , \
     '../scores/avg-model1NN-100.txt' , \
     '../scores/avg-model2NN-100.txt' , \
     '../scores/avg-model3NN-100.txt' , \
     '../scores/avg-model4NN-100.txt' , \
     '../scores/avg-model5NN-100.txt' 
