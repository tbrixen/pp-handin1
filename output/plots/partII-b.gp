set title "Cumulative error function for emperical fingerprinting"
set xrange [0:15]
set yrange [0:1]
#set autoscale
set grid
set style data linespoints
set key reverse Left outside
set terminal png size 800,300 
set output "../plots/partII-b.png"
plot '../scores/empirical1NN.txt' , \
     '../scores/empirical3NN.txt' , \
     '../scores/model1NN.txt' , \
     '../scores/model3NN.txt'
