set title "Cumulative error function for 100 iterations of empirical"
set autoscale
set grid
set style data linespoints
set key reverse Left outside
set terminal png size 1000,500 
set output "../plots/partII-c-e.png"
plot '../scores/avg-empirical1NN-100.txt' , \
     '../scores/avg-empirical2NN-100.txt' , \
     '../scores/avg-empirical3NN-100.txt' , \
     '../scores/avg-empirical4NN-100.txt' , \
     '../scores/avg-empirical5NN-100.txt'
