set title "Signal strength relating to distance"
set autoscale
set grid
set style data linespoints
set key reverse Left outside
set terminal png size 800,300 
set output "../plots/partII-a.png"
plot '../scores/SSAvg.txt' , \
     '../scores/SSMax.txt' , \
     '../scores/SSMin.txt'
