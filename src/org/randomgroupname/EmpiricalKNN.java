package org.randomgroupname;

import java.io.*;
import java.util.*;

import org.pi4.locutil.GeoPosition;
import org.pi4.locutil.MACAddress;
import org.pi4.locutil.io.TraceGenerator;
import org.pi4.locutil.trace.Parser;
import org.pi4.locutil.trace.TraceEntry;

/**
 * Example of how to use LocUtil
 * @author mikkelbk
 */

public class EmpiricalKNN {

    private List<TraceEntry> onlineTrace;
    private List<TraceEntry> offlineTrace;
    private String outputFilename = null;

    public EmpiricalKNN() {

        String offlinePath = "data/MU.1.5meters.offline.trace", onlinePath = "data/MU.1.5meters.online.trace";

        //Construct parsers
        File offlineFile = new File(offlinePath);
        Parser offlineParser = new Parser(offlineFile);
        System.out.println("#Offline File: " +  offlineFile.getAbsoluteFile());

        File onlineFile = new File(onlinePath);
        Parser onlineParser = new Parser(onlineFile);
        System.out.println("#Online File: " + onlineFile.getAbsoluteFile());

        //Construct trace generator
        TraceGenerator tg;
        try {
            int offlineSize = 25;
            int onlineSize = 5;
            tg = new TraceGenerator(offlineParser, onlineParser, offlineSize, onlineSize);

            //Generate traces from parsed files
            tg.generate();

            onlineTrace = tg.getOnline();
            offlineTrace = tg.getOffline();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
    }


    public void setOfflineTrace(List<TraceEntry> offlineTrace){
        this.offlineTrace = offlineTrace;
    }
    /**
     * Finds distinct geopositions from a trace
     * @param offlineTrace
     * @return
     */
    public List<GeoPosition> getUniqueGeoPos(List<TraceEntry> offlineTrace){
        //Iterate the trace generated from the offline file
        List<GeoPosition> uG = new ArrayList<GeoPosition>();
        for(TraceEntry entry: offlineTrace) {
            //Print out coordinates for the collection point and the number of signal strength samples
            //System.out.println(entry.getGeoPosition().toString() + " - " + entry.getSignalStrengthSamples().size());

            if (!uG.contains(entry.getGeoPosition())){
                uG.add(entry.getGeoPosition());
            }
        }
        return uG;
    }

    /**
     * Takes trace, and a set of geopositions. Returns, for each geoposition an avarage of each signal strength
     * @param offlineTrace
     * @param uG unique geopositions
     * @return
     */
    public Map<GeoPosition, List<TraceEntry>> divEntriesOnGeoPos(List<TraceEntry> offlineTrace, List<GeoPosition> uG ){
        Map<GeoPosition, List<TraceEntry>> tM = new HashMap<GeoPosition, List<TraceEntry>>();
        for (GeoPosition g : uG){
            List<TraceEntry> entries = new ArrayList<TraceEntry>();

            for (TraceEntry entry : offlineTrace){
                if (g.equals(entry.getGeoPosition())){
                    entries.add(entry);
                }
            }

            tM.put(g,entries);
        }
        return tM;
    }

    public Map<GeoPosition, Map<MACAddress, Double>> getMacAvgSsOnGeoPos(List<GeoPosition> uG,Map<GeoPosition, List<TraceEntry>> tM){
        Map<GeoPosition, Map<MACAddress, Double>> mASOGP = new HashMap<GeoPosition, Map<MACAddress, Double>>();
        for (GeoPosition g : uG) {
            mASOGP.put(g, getMacAvgSs(tM, g));
        }

        return mASOGP;
    }

    public Map<MACAddress, Double> getMacAvgSs(Map<GeoPosition, List<TraceEntry>> traceMap, GeoPosition g){
        Map<MACAddress, Double> macSS = new HashMap<MACAddress, Double>();

        macSS.put(MACAddress.parse("00:14:BF:B1:7C:54"), 0.0);
        macSS.put(MACAddress.parse("00:16:B6:B7:5D:8F"), 0.0);
        macSS.put(MACAddress.parse("00:14:BF:B1:7C:57"), 0.0);
        macSS.put(MACAddress.parse("00:14:BF:B1:97:8D"), 0.0);
        macSS.put(MACAddress.parse("00:16:B6:B7:5D:9B"), 0.0);
        macSS.put(MACAddress.parse("00:14:6C:62:CA:A4"), 0.0);
        macSS.put(MACAddress.parse("00:14:BF:3B:C7:C6"), 0.0);
        macSS.put(MACAddress.parse("00:14:BF:B1:97:8A"), 0.0);
        macSS.put(MACAddress.parse("00:14:BF:B1:97:81"), 0.0);
        macSS.put(MACAddress.parse("00:16:B6:B7:5D:8C"), 0.0);
        macSS.put(MACAddress.parse("00:11:88:28:5E:E0"), 0.0);

        List<TraceEntry> entryList = traceMap.get(g);
        //System.out.print(g.toString());
        for (TraceEntry te : entryList){
            List<MACAddress> macList = te.getSignalStrengthSamples().getSortedAccessPoints();

            for(MACAddress mac : macList){
                Double sS = te.getSignalStrengthSamples().getSignalStrengthValues(mac).get(0);
                if (macSS.get(mac) == null) continue;

                if(macSS.get(mac) > -1.0){
                    macSS.put(mac, sS);
                }else{
                    macSS.put(mac, ((macSS.get(mac)+sS)/2));
                }
            }
            //System.out.println(te.getSignalStrengthSamples().getSignalStrengthValues(te.getSignalStrengthSamples().getSortedAccessPoints().get(0)).get(0));
            //System.out.println(te.getSignalStrengthSamples().getSignalStrengthSamples(te.getSignalStrengthSamples().getSortedAccessPoints().get(0)));
        }

        for (Map.Entry<MACAddress, Double> ma: macSS.entrySet()){
            if(ma.getValue() > -1.0){
                ma.setValue(-110.0);
            }

            //System.out.print(g.toString());
            //System.out.println(" - " + ma);
        }

        return macSS;
    }

    public List<Pair<GeoPosition, Double>> createResultPairs(Map<GeoPosition, Map<MACAddress, Double>> macsAvgSsOnGeo, Map<MACAddress, Double> online){
        List<Pair<GeoPosition, Double>> resultPairs = new ArrayList<Pair<GeoPosition, Double>>();

        for(Map.Entry<GeoPosition, Map<MACAddress, Double>> pairs: macsAvgSsOnGeo.entrySet()){
            Map<MACAddress, Double> offline = pairs.getValue();

            Double innerVal = 0.0;
            for(Map.Entry<MACAddress, Double> md : online.entrySet()){
                if(md.getValue() == 0.0) continue;
                Double offlineVal = offline.get(md.getKey());
                innerVal += Math.pow((md.getValue() - offlineVal), 2);
            }
            GeoPosition offlinePos = pairs.getKey();
            Double res = Math.sqrt(innerVal);
            resultPairs.add(new Pair<GeoPosition, Double>(offlinePos, res));
        }

        return resultPairs;
    }

    public GeoPosition getBestPos(Map<GeoPosition, Double> resultPairs){
        GeoPosition bestPos = new GeoPosition();
        Double bestScore = 110.0;

        for(Map.Entry<GeoPosition, Double> gd : resultPairs.entrySet()){
            if(gd.getValue()< bestScore){
                bestPos = gd.getKey();
                //bestScore = gd.getValue();
            }
        }
        return bestPos;
    }

    public void sortList(List<Pair<GeoPosition, Double>> rs){
        Collections.sort(rs, new Comparator<Pair<GeoPosition, Double>>() {
            @Override
            public int compare(Pair<GeoPosition, Double> o, Pair<GeoPosition, Double> o2) {
                return (Double.compare(o.getValue(), o2.getValue()));
            }
        });
    }

   /* public List<GeoPosition> getPos(List<Pair<GeoPosition, Double>> input)    {
        List<GeoPosition> output = new ArrayList<GeoPosition>();
        for(Pair<GeoPosition, Double> pair : input){
            output.add(pair.getKey());
        }
        return output;
    }*/

    public GeoPosition estimatePos(TraceEntry entry, Map<GeoPosition, Map<MACAddress, Double>> macsAvgSsOnGeo, int k){
        Map<MACAddress, Double> online = new HashMap<MACAddress, Double>();

        online.put(MACAddress.parse("00:14:BF:B1:7C:54"), 0.0);
        online.put(MACAddress.parse("00:16:B6:B7:5D:8F"), 0.0);
        online.put(MACAddress.parse("00:14:BF:B1:7C:57"), 0.0);
        online.put(MACAddress.parse("00:14:BF:B1:97:8D"), 0.0);
        online.put(MACAddress.parse("00:16:B6:B7:5D:9B"), 0.0);
        online.put(MACAddress.parse("00:14:6C:62:CA:A4"), 0.0);
        online.put(MACAddress.parse("00:14:BF:3B:C7:C6"), 0.0);
        online.put(MACAddress.parse("00:14:BF:B1:97:8A"), 0.0);
        online.put(MACAddress.parse("00:14:BF:B1:97:81"), 0.0);
        online.put(MACAddress.parse("00:16:B6:B7:5D:8C"), 0.0);
        online.put(MACAddress.parse("00:11:88:28:5E:E0"), 0.0);

        for(MACAddress mac : online.keySet()){
            if(entry.getSignalStrengthSamples().containsKey(mac)){
                online.put(mac, entry.getSignalStrengthSamples().getSignalStrengthValues(mac).get(0));
            }else{
                online.put(mac, 0.0);
            }
        }

        List<Pair<GeoPosition, Double>> resultPairs = createResultPairs(macsAvgSsOnGeo, online);

        sortList(resultPairs);

        List<Pair<GeoPosition, Double>> kBest = resultPairs.subList(0, k);

        Double accSs = 0.0;
        for(Pair<GeoPosition, Double> gd: kBest){
            accSs += 1/gd.getValue();
        }
        Double avgSs = accSs/k;

        Map<GeoPosition, Double> ratios = new HashMap<GeoPosition, Double>();
        Double accRatios = 0.0;
        //int i = 1;
        for(Pair<GeoPosition, Double> gd: kBest){
            ratios.put(gd.getKey(), gd.getValue() / avgSs);
            accRatios += gd.getValue()/avgSs;
            //System.out.println("r" + i + ": " + gd.getValue()/avgSs);
            //i++;
        }

        //System.out.println("accRatio: " + accRatios);

        Double scale = 1/accRatios;
        //System.out.println("Scalefactor: " + scale);

        Double x = 0.0;
        Double y = 0.0;
        Double z = 0.0;
        for(Map.Entry<GeoPosition, Double> gp : ratios.entrySet()){
            x += gp.getKey().getX() * gp.getValue() * scale;
            y += gp.getKey().getY() * gp.getValue() * scale;
            z += gp.getKey().getZ() * gp.getValue() * scale;
        }

        GeoPosition bestPos = new GeoPosition(x, y, z);
        //System.out.println("Scale: " + scale);

        return bestPos;
    }


    public void empFPNN(int k){

        // If the filename has not been set
        if (outputFilename == null){
            outputFilename = "empirical" + k + "NN.txt";
        }

        try {
            // This section groups the entries on the their geo position

            // First get a list of unique geo positions
            List<GeoPosition> uniqueGeo = getUniqueGeoPos(offlineTrace);//new ArrayList<GeoPosition>();

            // We divide the entries into their respective groups dependent on the geoposition
            Map<GeoPosition, List<TraceEntry>> traceMap = divEntriesOnGeoPos(offlineTrace, uniqueGeo);//new HashMap<GeoPosition, List<TraceEntry>>();

            // For each position, we create a new TraceEntry with the average signal strength.
            Map<GeoPosition, Map<MACAddress, Double>> macsAvgSsOnGeo = getMacAvgSsOnGeoPos(uniqueGeo, traceMap);//new HashMap<GeoPosition, Map<MACAddress, Double>>();

            //Iterate the trace generated from the online file
            String result = "# True pos - Estimated pos" + System.getProperty("line.separator");
            for(TraceEntry entry: onlineTrace) {

                GeoPosition onlinePos = entry.getGeoPosition();
                GeoPosition bestPos = estimatePos(entry, macsAvgSsOnGeo, k);

                result += onlinePos + " - " + bestPos + System.getProperty("line.separator");
            }

            System.out.println(result);
            write(result);

        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
    }


    public void setOutputFilename(String filename){
        outputFilename = filename;
    }

    private void write(String output) {
        try {
            PrintWriter writer = new PrintWriter("output/" + outputFilename, "UTF-8");
            writer.println(output);
            writer.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    /**
     * Execute example
     * @param args
     */
    public static void main(String[] args) {

        EmpiricalKNN lUE = new EmpiricalKNN();

        if(args.length != 1){
            System.out.println("Please supply a k for k-nearest neighbor algorithm");
            return;
        }

        lUE.empFPNN(Integer.parseInt(args[0]));
    }
}
