package org.randomgroupname;


import org.pi4.locutil.GeoPosition;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Reads a result file, og generates a new file with the the errordistance, and number of readings that have the same
 * errorrate.
 *
 * Created by tobias on 9/13/14.
 */
public class Avg {

    public static void main(String[] args){
        // What input did we get
        if (args.length != 1){
            System.out.println("You need to provide file argument");
            return;
        }
        String filepath = args[0];

        Avg avg = new Avg(filepath);
        avg.go();
    }



    private String inputFilePath;
    private String outputFilePath;
    private Map<GeoPosition, List<GeoPosition>> map = new HashMap<GeoPosition, List<GeoPosition>>();
    private Map<GeoPosition, GeoPosition> resultMap = new HashMap<GeoPosition, GeoPosition>();

    private String output;


    public Avg(String filename){
        inputFilePath = "output/" + filename;
        outputFilePath = "output/avg-" + filename;

        output  = "# Output from Avg.java with " + inputFilePath + System.getProperty("line.separator");
        output += "# From " + new SimpleDateFormat("dd/MM/yy HH:mm:ss").format(Calendar.getInstance().getTime()) + System.getProperty("line.separator");
        output += "# Error-distance, percentage of distances which are <= this error distance";
    }

    public void go(){
        parse();
        calculateAverage();
        write();
    }

    private void calculateAverage() {
       for (Map.Entry<GeoPosition, List<GeoPosition>> lists : map.entrySet()) {
           List<GeoPosition> list = lists.getValue();
           int size = list.size();

           Double totalX = 0.0;
           Double totalY = 0.0;


           for (GeoPosition gp : list){
               totalX += gp.getX();
               totalY += gp.getY();
           }

           GeoPosition newGeo = new GeoPosition(totalX / size, totalY/size);

           resultMap.put(lists.getKey(), newGeo);
       }
    }

    private void parse() {
        File file = new File(inputFilePath);
        String line;


        try {
            BufferedReader reader = new BufferedReader(new FileReader(file));


            while ((line = reader.readLine()) != null){
                // Skip comment block
                if (line.startsWith("#") || line.isEmpty())
                    continue;


                // Convert to a form of CSV
                // Source form: (-23.9, -9.65, 0.0, NaN) - (-18.0, -9.75, 0.0, NaN)
                line = line.replace("(","");
                line = line.replace(")","");
                line = line.replace(" - ",",");
                line = line.replace(" ", "");


                // Line now has the form
                // trueX, trueY, trueZ, NaN, estiX, estiY, estyZ, NaN
                String[] parts = line.split(",");

                double trueX = Double.parseDouble(parts[0]);
                double trueY = Double.parseDouble(parts[1]);
                double estiX = Double.parseDouble(parts[4]);
                double estiY = Double.parseDouble(parts[5]);


                GeoPosition trueGP = new GeoPosition(trueX, trueY);
                GeoPosition estiGP = new GeoPosition(estiX, estiY);


                // If the map contains an entry for the true GP, add this
                // otherwise add it.
                List<GeoPosition> geoList = map.get(trueGP);
                if (geoList == null){
                    geoList = new ArrayList<GeoPosition>();
                    geoList.add(estiGP);
                    map.put(trueGP, geoList);
                } else {
                    geoList.add(estiGP);
                }
            }
        } catch (FileNotFoundException e) {
            System.out.println("File " + file.getAbsoluteFile() + " was not found");
            e.printStackTrace();
        } catch (IOException e){
            e.printStackTrace();
        }
    }

    private void write() {

        try {
            PrintWriter writer = new PrintWriter(outputFilePath, "UTF-8");

            writer.println(output);

            for (Map.Entry<GeoPosition, GeoPosition> gp : resultMap.entrySet()){
                writer.println(gp.getKey().toString() + " - " + gp.getValue().toString());
            }

            writer.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }
}
