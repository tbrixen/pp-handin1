package org.randomgroupname;


import java.io.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;

/**
 * Reads a result file, og generates a new file with the the errordistance, and number of readings that have the same
 * errorrate.
 *
 * Created by tobias on 9/13/14.
 */
public class ScoreNN {

    public static void main(String[] args){
        // What input did we get
        if (args.length != 1){
            System.out.println("You need to provide file argument");
            return;
        }
        String filepath = args[0];


        ScoreNN scoreNN = new ScoreNN(filepath);
        scoreNN.go();

        //ScoreNN a = new ScoreNN("100accAvg.txt");
        //a.go();
        //ScoreNN score1NN = new ScoreNN("empirical1NN.txt");
        //score1NN.go();
        //ScoreNN score3NN = new ScoreNN("emperical3NN.txt");
        //score3NN.go();
    }



    private String inputFilePath;
    private String outputFilePath;
    private List<Double> errorList = new ArrayList<Double>();
    private String output;
    private String EOL = System.getProperty("line.separator");


    public ScoreNN(String filename){
        inputFilePath = "output/" + filename;
        outputFilePath = "output/scores/" + filename;


        output  = "# Output from " + EOL;
        output += "# From " + new SimpleDateFormat("dd/MM/yy HH:mm:ss").format(Calendar.getInstance().getTime()) + EOL;
        output += "# Error-distance, percentage of distances which are <= this error distance" + EOL;
    }

    public void go(){
        parse();
        calculateCumulativeErrorDistance();
        write();
    }

    private void parse() {
        File file = new File(inputFilePath);
        String line;


        try {
            BufferedReader reader = new BufferedReader(new FileReader(file));


            while ((line = reader.readLine()) != null){
                // Skip comment block
                if (line.startsWith("#") || line.isEmpty())
                    continue;


                // Convert to a form of CSV
                // Source form: (-23.9, -9.65, 0.0, NaN) - (-18.0, -9.75, 0.0, NaN)
                line = line.replace("(","");
                line = line.replace(")","");
                line = line.replace(" - ",",");
                line = line.replace(" ", "");


                // Line now has the form
                // trueX, trueY, trueZ, NaN, estiX, estiY, estyZ, NaN
                String[] parts = line.split(",");

                double trueX = Double.parseDouble(parts[0]);
                double trueY = Double.parseDouble(parts[1]);
                double estiX = Double.parseDouble(parts[4]);
                double estiY = Double.parseDouble(parts[5]);

                // Calculate and push the errordistance to the list
                pushToErrorList(trueX, trueY, estiX, estiY);
            }
        } catch (FileNotFoundException e) {
            System.out.println("File " + file.getAbsoluteFile() + " was not found");
            e.printStackTrace();
        } catch (IOException e){
            e.printStackTrace();
        }
    }


    private void calculateCumulativeErrorDistance(){
        Collections.sort(errorList);

        // Calculate the cumulative distribution, and append this to the output.
        Double lastSeen = 0.0;
        int count = 0;
        double total = errorList.size();

        for (double d : errorList){
            System.out.println(d);
            // If they are not equal, we output the percentage of how many are equal of less than this error distance
            if (Double.compare(d, lastSeen) != 0){
                // output the percentage
                double percentage = count / total;
                writeToResult(lastSeen, percentage);
                lastSeen = d;
            }
            count++;
        }
        // And get the last one
        double percentage = count / total;
        writeToResult(lastSeen, percentage);


    }

    private void write() {
        try {
            PrintWriter writer = new PrintWriter(outputFilePath, "UTF-8");
            writer.println(output);
            writer.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    private void writeToResult(Double lastSeen, double percentage) {
        output += lastSeen.toString() + " " + percentage + EOL;
    }

    /**
     * Calculates the distance and pushes to the errorlist.
     * @param trueX double of the true X position
     * @param trueY double of the true Y position
     * @param estiX double of the estimated X position
     * @param estiY double of the estimated Y position
     */
    private void pushToErrorList(double trueX, double trueY, double estiX, double estiY) {
        // Calculate the error
        double errorDistance = Math.sqrt(
                Math.pow(trueX - estiX, 2) +
                Math.pow(trueY - estiY, 2));

        errorList.add(errorDistance);
    }

}
