package org.randomgroupname;

import java.io.*;
import java.util.*;

import org.pi4.locutil.GeoPosition;
import org.pi4.locutil.MACAddress;
import org.pi4.locutil.io.TraceGenerator;
import org.pi4.locutil.trace.Parser;
import org.pi4.locutil.trace.TraceEntry;

/**
 * Example of how to use LocUtil
 * @author mikkelbk
 */

public class SignalStrength {

    private List<TraceEntry> traces;

    public SignalStrength() {

        distance_to_signal = new HashMap<Integer, List<Double>>(); 

        String offlinePath = "data/MU.1.5meters.offline.trace", onlinePath = "data/MU.1.5meters.online.trace";

        //Construct parsers
        File offlineFile = new File(offlinePath);
        Parser offlineParser = new Parser(offlineFile);
        System.out.println("Offline File: " +  offlineFile.getAbsoluteFile());

        File onlineFile = new File(onlinePath);
        Parser onlineParser = new Parser(onlineFile);
        System.out.println("Online File: " + onlineFile.getAbsoluteFile());

        //Construct trace generator
        TraceGenerator tg;
        try {
            int offlineSize = 25;
            int onlineSize = 5;
            tg = new TraceGenerator(offlineParser, onlineParser, offlineSize, onlineSize);

            //Generate traces from parsed files
            tg.generate();

            traces = tg.getOnline();
            traces.addAll(tg.getOffline());
        } catch (IOException e1) {
            e1.printStackTrace();
        }
    }

    private GeoPosition get_position_by_mac(MACAddress mac, List<TraceEntry> collection)
    {
        for(TraceEntry entry : collection)
        {
            if(entry.getId().equals(mac))
            {
                return entry.getGeoPosition();
            }
        }
        return null;
    }

    private List<TraceEntry> access_points()
    {
        List<TraceEntry> APs = new ArrayList<TraceEntry>();
        APs.add(new TraceEntry(0,new GeoPosition(-23.626,-18.596),MACAddress.parse("00:14:BF:B1:7C:54"),null));
        APs.add(new TraceEntry(0,new GeoPosition(-10.702, -18.595),MACAddress.parse("00:16:B6:B7:5D:8F"),null));
        APs.add(new TraceEntry(0,new GeoPosition(8.595,-14.62),MACAddress.parse("00:14:BF:B1:7C:57"),null));
        APs.add(new TraceEntry(0,new GeoPosition(8.538,-9.298),MACAddress.parse("00:14:BF:B1:97:8D"),null));
        APs.add(new TraceEntry(0,new GeoPosition(-1.93, -2.749),MACAddress.parse("00:16:B6:B7:5D:9B"),null));
        APs.add(new TraceEntry(0,new GeoPosition(4.035, -0.468),MACAddress.parse("00:14:6C:62:CA:A4"),null));
        APs.add(new TraceEntry(0,new GeoPosition(13.333, -2.69),MACAddress.parse("00:14:BF:3B:C7:C6"),null));
        APs.add(new TraceEntry(0,new GeoPosition(21.17, -2.69),MACAddress.parse("00:14:BF:B1:97:8A"),null));
        APs.add(new TraceEntry(0,new GeoPosition(32.398, -2.69),MACAddress.parse("00:14:BF:B1:97:81"),null));
        APs.add(new TraceEntry(0,new GeoPosition(32.573, 13.86),MACAddress.parse("00:16:B6:B7:5D:8C"),null));
        APs.add(new TraceEntry(0,new GeoPosition(7.135, 6.023),MACAddress.parse("00:11:88:28:5E:E0"),null));
        return APs;
    }

    private boolean is_access_point(MACAddress mac)
    {
        for(TraceEntry entry : access_points())
        {
            if(entry.getId().equals(mac))
            {
                return true;
            }
        }
        return false;
    }

    private int roundUp(int n, int base) {
        return ((n + (base-1)) / base) * base;
    }

    Map<Integer, List<Double>> distance_to_signal; 

    private void append_data(double signal, double distance)
    {
        int rounded_distance = roundUp((int)distance, 5);
        List<Double> signals = distance_to_signal.get(rounded_distance);
        if(signals == null)
        {
            signals = new ArrayList<Double>();
        }
        signals.add(signal);
        distance_to_signal.put(rounded_distance, signals);
    }

    private void calculate()
    {
        for(TraceEntry entry : traces)
        {
            GeoPosition actual_pos = entry.getGeoPosition();
            
            List<MACAddress> mac_list = entry.getSignalStrengthSamples().getSortedAccessPoints();
            for(MACAddress mac : mac_list)
            {
                // Gets the geoposition of the macaddress.
                GeoPosition access_pos = get_position_by_mac(mac, access_points());
                if(access_pos == null)
                {
                    if(is_access_point(mac))
                        System.out.println("WHAT");
                    continue;
                }
                Double signal = entry.getSignalStrengthSamples().getSignalStrengthValues(mac).get(0);
                Double distance = actual_pos.distance(access_pos);
                //System.out.println("(" + actual_pos.getX() + " , " + actual_pos.getY() + ")" + " - " +
                //                   "(" + access_pos.getX() + " , " + access_pos.getY() + ")");
                //System.out.println(signal + " - " + distance);
                append_data(signal, distance);
            }
        }


        String EOL = System.getProperty("line.separator");
        String minResult = "";
        String maxResult = "";
        String avgResult = "";

        for(Map.Entry<Integer, List<Double>> interval : distance_to_signal.entrySet())
        {
            Double min = Collections.min(interval.getValue());
            Double max = Collections.max(interval.getValue());
            double avg = 0;
            for(Double d : interval.getValue())
            {
                avg += d;
            }
            avg /= interval.getValue().size();

            minResult += interval.getKey() + " " + min + EOL;
            maxResult += interval.getKey() + " " + max + EOL;
            avgResult += interval.getKey() + " " + avg + EOL;

        }

        write(minResult, "output/scores/signalStrMin.txt");
        write(maxResult, "output/scores/signalStrMax.txt");
        write(avgResult, "output/scores/signalStrAvg.txt");

    }

    private void write(String output, String filePath) {
        try {
            PrintWriter writer = new PrintWriter(filePath, "UTF-8");
            writer.println(output);
            writer.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }
    /**
     * Execute example
     * @param args
     */
    public static void main(String[] args) {

        SignalStrength ss = new SignalStrength();

        ss.calculate();
    }
}
