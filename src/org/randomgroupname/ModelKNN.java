package org.randomgroupname;

import org.pi4.locutil.GeoPosition;
import org.pi4.locutil.MACAddress;
import org.pi4.locutil.trace.SignalStrengthSamples;
import org.pi4.locutil.trace.TraceEntry;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by tobias on 9/14/14.
 */
public class ModelKNN {

    private List<TraceEntry> radioMap = null;
    int k;

    public ModelKNN(int k){
        this.k = k;
    }

    public List<TraceEntry> getRadioMap(){
        if (radioMap == null){
            createRadioMap();
        }

        return radioMap;
    }
    private void createRadioMap(){
        radioMap =  new ArrayList<TraceEntry>();

        // All the known APs
        List<TraceEntry> APs = new ArrayList<TraceEntry>();
        APs.add(new TraceEntry(0,new GeoPosition(-23.626,-18.596),MACAddress.parse("00:14:BF:B1:7C:54"),null));
        APs.add(new TraceEntry(0,new GeoPosition(-10.702, -18.595),MACAddress.parse("00:16:B6:B7:5D:8F"),null));
        APs.add(new TraceEntry(0,new GeoPosition(8.595,-14.62),MACAddress.parse("00:14:BF:B1:7C:57"),null));
        APs.add(new TraceEntry(0,new GeoPosition(8.538,-9.298),MACAddress.parse("00:14:BF:B1:97:8D"),null));
        APs.add(new TraceEntry(0,new GeoPosition(-1.93, -2.749),MACAddress.parse("00:16:B6:B7:5D:9B"),null));
        APs.add(new TraceEntry(0,new GeoPosition(4.035, -0.468),MACAddress.parse("00:14:6C:62:CA:A4"),null));
        APs.add(new TraceEntry(0,new GeoPosition(13.333, -2.69),MACAddress.parse("00:14:BF:3B:C7:C6"),null));
        APs.add(new TraceEntry(0,new GeoPosition(21.17, -2.69),MACAddress.parse("00:14:BF:B1:97:8A"),null));
        APs.add(new TraceEntry(0,new GeoPosition(32.398, -2.69),MACAddress.parse("00:14:BF:B1:97:81"),null));
        APs.add(new TraceEntry(0,new GeoPosition(32.573, 13.86),MACAddress.parse("00:16:B6:B7:5D:8C"),null));
        APs.add(new TraceEntry(0,new GeoPosition(7.135, 6.023),MACAddress.parse("00:11:88:28:5E:E0"),null));




        // For each integer position on the map, create a TraceEntry
        for (int x = -23; x <= 32; x++){
            for (int y = -18; y <= 13; y++) {

                // Create the positions, and a container for signalstrengths
                GeoPosition geoPosition = new GeoPosition(x, y);
                SignalStrengthSamples ssSamples = new SignalStrengthSamples();

                // Create our trace-entry.
                TraceEntry te = new TraceEntry(0, geoPosition, MACAddress.parse("DE:AD:BE:EF:DE:AD"), ssSamples);

                // For each AP, calculate the estimated Signal Strength from the our geo position
                for (TraceEntry ap : APs) {
                    // Get the position of the current ap.
                    GeoPosition APGeo = ap.getGeoPosition();

                    // Get distance in meters from AP to our position
                    Double d = Math.sqrt(
                                    Math.pow(x - APGeo.getX(), 2) +
                                    Math.pow(y - APGeo.getY(), 2));

                    // Constants for the estimate
                    Double Pd0 = -33.77;
                    Double n = 3.415;
                    Double d0 = 1.0;

                    // Calculate the estimated position
                    Double estimatedSS = Pd0 - 10 * n * Math.log10(d / d0);

                    // Add this estimate to the signalstrengths
                    ssSamples.put(ap.getId(), estimatedSS);
                }

                // Add this position to the radiomap.
                radioMap.add(te);
            }
        }
    }
    public static void main(String[] args){

        if (args.length != 1){
            System.out.println("You need 1 parameter.");
            System.out.println("Usage: ");
            System.out.println("\t java org.randomgroupname.modelKNN [k]");
            return;
        }

        int k = Integer.parseInt(args[0]);

        // Calculate the radiomap
        ModelKNN model = new ModelKNN(k);
        model.createRadioMap();
        List<TraceEntry> radioMap = model.getRadioMap();

        // Feed this radiomap into the empirical (this is the same algorithm)
        EmpiricalKNN empiricalKNN = new EmpiricalKNN();
        empiricalKNN.setOfflineTrace(radioMap);
        empiricalKNN.setOutputFilename("model" + k + "NN.txt");
        empiricalKNN.empFPNN(k);
    }
}
